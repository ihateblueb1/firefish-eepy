# Changes to the Firefish API

Breaking changes are indicated by the :warning: icon.

## v1.0.5 (unreleased)

### dev17

- Added `lang` parameter to `notes/create` and `notes/edit`.

### dev11

- :warning: `notes/translate` now requires credentials.
